﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using testDB.Models;

namespace testDB.Interfaces
{
    interface IUserRepository
    {
        public Task<List<User>> getAll();
        public Task<User> getByFullName(string firstName, string lastName);
        public Task<User> getById(int id);
        public Task<bool> createUser(User user);
    }
}
