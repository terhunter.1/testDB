﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using testDB.Models;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using testDB.Interfaces;

namespace testDB.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationContext _db;
        private readonly ILogger<UserRepository> _logger;

        public UserRepository(ApplicationContext db, ILogger<UserRepository> logger) { 
            _db = db;
            _logger = logger;
        }

        public async Task<List<User>> getAll() {
            return await _db.Users.ToListAsync();
        }

        public async Task<User> getByFullName(string firstName, string lastName) {
            string newFullName = firstName.ToLower() + " " + lastName.ToLower();
            _logger.LogTrace(newFullName);

            return await _db.Users.FirstOrDefaultAsync(user => user.FirstName.ToLower() + " " + user.LastName.ToLower() == newFullName);
        }

        public async Task<User> getById(int id) {
            return await _db.Users.FirstOrDefaultAsync(user => user.Id == id);
        }

        public async Task<bool> createUser(User user) {
            User filteredData = await getByFullName(user.FirstName, user.LastName);
            if (filteredData != null) {
                _logger.LogError("User name is exists. Creation impossible");
                return false;
            }
            _db.Users.Add(user);
            await _db.SaveChangesAsync();

            return true;
        }
    }
}
