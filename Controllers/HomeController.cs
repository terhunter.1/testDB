﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Collections.Generic;
using System.Threading.Tasks;
using testDB.Models;
using testDB.Repositories;

namespace testDB.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private UserRepository _userRepository;

        public HomeController(
            UserRepository userRepository,
            ILogger<HomeController> logger
        )
        {
            _logger = logger;
            _userRepository = userRepository;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Users()
        {
            List<User> users = await _userRepository.getAll();
            return View(users);
        }
        public IActionResult CreateUser()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser(User user) {
            bool result = await _userRepository.createUser(user);
            _logger.LogTrace("Create user is call");
            if (!result)
            {
                TempData["alertMessage"] = "User already exists";
                return View();
            }
            ViewBag.Message = null;
            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
